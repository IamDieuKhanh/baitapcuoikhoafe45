import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import movies from "../redux/reducers/movies";
//cai đặt middleware => thunk
import thunk from "redux-thunk";
//create root reducer
const reducer = combineReducers({
  movies,
});

export const store = createStore(
  reducer,
  compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);
