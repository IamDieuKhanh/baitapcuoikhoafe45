/*
Ngày bắt đầu làm: T2 21/9/2020
Người khởi tạo: Quách Diệu Khánh
*/
import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import CarouselComponent from "../../components/Carousel";
import ControlMovieList from "../../components/ControlMovieList";
import Header from "../../components/Header";
import { fetchMovie } from "../../redux/actions/movies";
import { connect } from "react-redux";

class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <CarouselComponent />
        <ControlMovieList />
      </div>
    );
  }
  //Call API render movie
  componentDidMount() {
    this.props.dispatch(fetchMovie);
  }
}

export default connect()(Home);
