import {
  Box,
  Button,
  Container,
  TextField,
  withStyles,
  withTheme,
} from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";

import React, { Component } from "react";
import classes from "./style.module.css";
import styles from "./style";
import { Link } from "react-router-dom";
class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundImg: "./image/bg2.jpg",
    };
  }

  render() {
    return (
      <div
        style={{
          background: `url(${this.state.backgroundImg})`,
          backgroundSize: "cover",
          backgroundAttachment: "fixed",
          height: 800,
        }}
      >
        <Container maxWidth="sm">
          <div className={classes.bgLoginSite}>
            <img src="./image/logo.png" className={classes.logo} />
            <div>
              <p>
                Đăng nhập để được nhiều ưu đãi, mua vé và bảo mật thông tin!
              </p>
            </div>
            <Container maxWidth="sm">
              <form>
                <Box my={3.1}>
                  <TextField
                    name="taikhoan"
                    variant="filled"
                    label="Username"
                    fullWidth
                    className={this.props.classes.textField}
                  />
                </Box>
                <Box my={3.1}>
                  <TextField
                    name="matkhau"
                    variant="filled"
                    label="Password"
                    fullWidth
                    type="password"
                    className={this.props.classes.textField}
                  />
                </Box>
                <Box textAlign="left">
                  <a href="">Quên mật khẩu?</a>
                  <br></br>
                  <a href="">Đăng kí tài khoản mới</a>
                </Box>
                <Box textAlign="left">
                  <Button
                    type="submit"
                    variant="contained"
                    color="secondary"
                    className={this.props.classes.btnSubmit}
                  >
                    Đăng nhập
                  </Button>
                </Box>
              </form>
              <Link to="/">
              <Button
                style={{marginTop: 50}}
                type="button"
                variant="contained"
                color="secondary"
                className={this.props.classes.btnBackToHome}
              >
                <ArrowBackIosIcon />
                Trở về trang chủ
              </Button>
              </Link>
            </Container>
          </div>
        </Container>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Signin);
