import theme from "../../theme";
const styles = () => {
  return {
    textField: {
      backgroundColor: theme.palette.background.white.light,
    },
    btnSubmit: {
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.text.default,
    },
    btnBackToHome: {
      backgroundColor: theme.palette.background.white.light,
      color: theme.palette.text.primary
    },
  };
};
export default styles;
