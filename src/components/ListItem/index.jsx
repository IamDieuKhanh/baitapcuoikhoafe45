import { Button, Card } from "react-bootstrap";

import React, { Component } from "react";
import Slider from "react-slick";
import classes from "./style.module.css";
import { Container } from "@material-ui/core";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";
import { connect } from "react-redux";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{
        ...style,
        display: "block",
        background: "black",
        borderRadius: 25,
      }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{
        ...style,
        display: "block",
        background: "black",
        borderRadius: 25,
      }}
      onClick={onClick}
    />
  );
}
class ListItem extends Component {
  renderMovies = () => {
    return this.props.movieList.map((item, index) => {
      const { tenPhim, hinhAnh, maNhom, ngayKhoiChieu,trailer } = item;
      return (
        <div key={index}>
          <Card
            className={classes.card}
            style={{ width: "13rem", border: "none" }}
          >
            <Card.Img
              variant="top"
              src={hinhAnh}
              className={classes.movieImg}
            />
            <Card.Body className={classes.cardText}>
              <Card.Text>
                <a className={classes.typeAge}>{maNhom}</a>
                {tenPhim.substr(0, 10) + "..."}
                <p>Ngày khởi chiếu: {ngayKhoiChieu}</p>
              </Card.Text>
            </Card.Body>

            <Card.Body className={classes.btnBuyTicket}>
              <Button variant="danger" size="lg" block>
                Mua vé
              </Button>
            </Card.Body>
            <div className={classes.cardOverlay}>
              <a href={trailer}><PlayCircleOutlineIcon className={classes.playButton}  style={{ fontSize: 60 }} /></a>
            </div>
          </Card>
        </div>
      );
    });
  };
  render() {
    var settings = {
      infinite: true,
      speed: 800,
      slidesToShow: 4,
      slidesToScroll: 4,
      initialSlide: 0,
      rows: 2,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      //Res
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
    return (
      <div>
        <Container maxWidth="md">
          <Slider {...settings}>
            {this.renderMovies()}
            {/* <div>
              <Card style={{ width: "13rem", border: "none" }}>
                <Card.Img variant="top" src="./image/phim.png" />
                <Card.Body className={classes.cardText}>
                  <Card.Text>
                    <a className={classes.typeAge}>C18</a> Some quick example
                    text
                  </Card.Text>
                  <p>100 phút</p>
                </Card.Body>
              </Card>
            </div> */}
          </Slider>
        </Container>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    movieList: state.movies.movieList,
  };
};
export default connect(mapStateToProps)(ListItem);
