import React, { Component } from "react";
import Slider from "react-slick";

import classes from "./style.module.css";

class PhoneSlider extends Component {
  render() {
    const settings = {
      dots: true,

      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      speed: 500,
      autoplaySpeed: 2000,
      cssEase: "linear",
    };
    return (
      <div>
        <Slider {...settings} >
          <div className={classes.phoneSlide}>
            <img src="./image/phone_slide_1.jpg" />
          </div>
          <div>
            <img src="./image/phone_slide_1.jpg" />
          </div>
          <div>
            <img src="./image/phone_slide_1.jpg" />
          </div>
          <div>
            <img src="./image/phone_slide_1.jpg" />
          </div>
        </Slider>
      </div>
    );
  }
}

export default PhoneSlider;
