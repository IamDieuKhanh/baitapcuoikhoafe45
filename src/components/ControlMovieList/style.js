import theme from "../../theme";
const style = () => {
  return {
    tabs: {
      color: theme.palette.text.secondary,
    },
  };
};

export default style;
