//async action
import { createAction } from "./index";
import { request } from "../../configs/request";
export const fetchMovie = (dispatch) => {
  request(
    "http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01",
    "GET"
  )
    .then((res) => {
      console.log(res);
      //dispatch action
      dispatch(createAction("SET_MOVIE", res.data));
    })
    .catch((err) => {
      console.log(err);
    });
};
