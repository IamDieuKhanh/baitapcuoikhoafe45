let initialState = {
  movieList: [],
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SET_MOVIE":
      state.movieList = payload;
      return { ...state };

    default:
      return state;
  }
};

export default reducer;
